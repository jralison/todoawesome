# @TodoAwesome
O @TodoAwesome é uma aplicação simples de gerenciamento de tarefas, com capacidade de compartilhar em tempo real, entre os usuários conectados à aplicação, as modificações realizadas.

O objetivo deste projeto é unicamente acadêmico, sendo criado especialmente para promover a prática pessoal com as tecnologias TypeScript, NodeJS e WebSockets.

Dicas e sugestões são sempre muito bem-vindas.
